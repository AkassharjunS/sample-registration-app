import React, {Component} from 'react'
import {Mutation} from 'react-apollo'
import gql from 'graphql-tag'
import {Query} from "@apollo/react-components";

const REGISTER_USER = gql`
    mutation RegisterUser($email: String!, $password: String!, $name: String!, $address : String!, $dateOfBirth: DateTime!) {
        registerUser(email: $email, password: $password, name: $name, address: $address, dateOfBirth: $dateOfBirth) {
            token
            user {
                id
            }
        }
    }
`

const USERS_QUERY = gql`
    {
        users {
            id
            email
            name
            address
        }
    }
`

class Login extends Component {
    state = {
        login: true, // switch between Login and SignUp
        email: '',
        password: '',
        name: '',
        dateOfBirth: Date.now().toString(),
        address: '',
        dob : ''
    }

    render() {
        const {login, email, password, name, address, dateOfBirth, dob} = this.state
        return (
            <div>
                <h4 className="mv3">{login ? 'Login' : 'Sign Up'}</h4>
                <div className="flex flex-column">
                    <input
                        value={name}
                        onChange={e => this.setState({name: e.target.value})}
                        type="text"
                        placeholder="Your name"
                    />
                    <input
                        value={email}
                        onChange={e => this.setState({email: e.target.value})}
                        type="text"
                        placeholder="Your email address"
                    />
                    <input
                        value={password}
                        onChange={e => this.setState({password: e.target.value})}
                        type="password"
                        placeholder="Choose a safe password"
                    />
                    <input
                        value={address}
                        onChange={e => this.setState({address: e.target.value})}
                        type="address"
                        placeholder="Enter your address"
                    />
                    <input
                        value={dob}
                        onChange={e => this.setState({dateOfBirth: new Date(e.target.value).toString(), dob:e.target.value})}
                        type="date"
                        placeholder="Enter your DOB"
                    />
                </div>
                <div className="flex mt3">
                    <Mutation
                        mutation={REGISTER_USER}
                        variables={{email, password, name, address, dateOfBirth}}
                        onCompleted={data => this._confirm(data)}
                    >
                        {mutation => (
                            <button type="button" onClick={mutation}>
                                LOGIN
                            </button>
                        )}
                    </Mutation>
                </div>
                <h1>Users fetched</h1>
                <Query query={USERS_QUERY}>
                    {({ loading, error, data }) => {
                        if (loading) return <div>Fetching</div>
                        if (error) return <div>Error</div>

                        const linksToRender = data.users

                        return (
                            <div>
                                {linksToRender.map(user => <h3>{user.name}</h3>)}
                            </div>
                        )
                    }}
                </Query>
                <div>{}</div>
            </div>
        )
    }

    _confirm = async data => {
        alert(data.registerUser.token);
    }

    _saveUserData = token => {
        localStorage.setItem("s", token)
    }
}

export default Login