const {GraphQLServer} = require("graphql-yoga");
const {prisma} = require('./generated/prisma-client')
const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')

// using a 3rd party package to define DateTime because GraphQL doesn't support DateTime
const GraphQLDateTime = require('graphql-type-datetime');

// defining the resolvers
const resolvers = {
    DateTime: GraphQLDateTime,
    Query,
    Mutation,
}


// defining the GraphQL server
const server = new GraphQLServer({
    typeDefs: "./schema.graphql",
    resolvers,
    context: request => {
        return {
            ...request,
            prisma,
        }
    },
});

server.start(() => console.log(`Server is running on http://localhost:4000`));
