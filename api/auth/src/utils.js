const jwt = require('jsonwebtoken')
// Should be an env variable
const APP_SECRET = 'GraphQL-is-aw3some'


// validates the request sent by the client
function validateUser(context) {
    const Authorization = context.request.get('Authorization')

    if (Authorization) {
        const token = Authorization.replace('Bearer ', '')
        return !!jwt.verify(token, APP_SECRET);
    }

    throw new Error('Not authenticated')
}

module.exports = {
    APP_SECRET,
    validateUser
}