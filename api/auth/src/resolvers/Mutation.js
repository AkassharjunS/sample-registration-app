const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {APP_SECRET, validateUser} = require('../utils')


// register a user
async function registerUser(parent, args, context, info) {
    const hashedPassword = await bcrypt.hash(args.password, 10)

    const {password, ...user} = await context.prisma.createUser({...args, password: hashedPassword})

    const token = jwt.sign({userId: user.id}, APP_SECRET)

    return {
        token,
        user,
    }
}

// log in a user
async function loginUser(parent, args, context, info) {

    const {password, ...user} = await context.prisma.user({email: args.email})

    if (!user) {
        throw new Error('No such user found')
    }

    const valid = await bcrypt.compare(args.password, password)

    if (!valid) {
        throw new Error('Invalid password')
    }

    const token = jwt.sign({userId: user.id}, APP_SECRET)

    return {
        token,
        user,
    }
}

// completely update user
async function updateUser(parent, args, context, info) {
    validateUser(context);

    if (typeof args.password === 'string') args.password = await bcrypt.hash(args.password, 10);

    return context.prisma.updateUser({
        where: {id: args.id},
        data: {
            name: args.name,
            password: args.password,
            address: args.address,
            dateOfBirth: args.dateOfBirth,
            email: args.email
        }
    });
}

// delete a user by id
function deleteUser(parent, args, context, info) {
    return context.prisma.deleteUser(
        {
            id: args.id
        }
    )
}

module.exports = {
    registerUser,
    loginUser,
    updateUser,
    deleteUser
}