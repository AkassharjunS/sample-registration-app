const {validateUser} = require('../utils')

// fetch all the users
function users(parent, args, context, info) {
    validateUser(context);
    return context.prisma.users();
}

// fetch user by id
async function user(parent, args, context, info) {
    validateUser(context);
    return context.prisma.user({
        id: args.id
    });
}

module.exports = {
    users,
    user
}