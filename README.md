# sample-registration-app

A sample application to demonstrate my knowledge in GraphQL with a touch of basic UI in React to visualize it.

## Requirements

- Node
- Docker (if you choose to run it locally)
